/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ApiContainerAPI from './Components/APIContainer'
import ListDetailed from './Components/ListDetailed'
import welcome from './Components/welcome'



const Stack = createStackNavigator();

function App() {
  return(
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="welcome" component={welcome} />
      <Stack.Screen name="Giphy gif" component={ApiContainerAPI} />
        <Stack.Screen name="gif Details" component={ListDetailed} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}



export default App;
