import React, { Component } from 'react';
import { Text, View, Image, AsyncStorage,TouchableHighlight } from 'react-native';
import styles from './ListStyle';
import { Card } from 'react-native-paper';

const dislikeIcon = require('../image/dislike.png');
const likeIcon = require('../image/like.png');




class ListDetailed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            favList: [],
            data: this.props.route.params.data
        };
    }

    render() {
        return (
            <View>
                <Card style={styles.card}>
                    <View style={{ justifyContent: "center", alignItems: "center", paddingBottom: 10 }}>
                        <Image style={styles.gif
                        }
                            source={{ uri: this.state.data.images.original.webp }}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'center', padding: 10, }}>
                        <Text style={{ fontWeight: "bold", fontSize: 16, }} >{this.state.data.title}</Text>
                    </View>

                    <View style={styles.dividerstyle} />

                    <View style={styles.viewstyle}>
                        <Text style={{ fontWeight: "bold", fontSize: 16}}>Rating : </Text>
                        <Text>{this.state.data.rating}</Text>
                    </View>

                    <View style={styles.dividerstyle} />

                    <View style={styles.viewstyle}>
                        <Text style={{ fontWeight: "bold", fontSize: 16}}>Source : </Text>
                        <Text>{this.state.data.source_tld}</Text>
                    </View>

                </Card>
            </View>
        );
    }
}

export default ListDetailed;